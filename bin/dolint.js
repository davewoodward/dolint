#!/usr/bin/env node

process.title = 'dolint';

/*
 * NOTE: The contents of this file should work on as many version of Node.js
 * as possible. This means it *can not* use any >ES5 syntax and features.
 * Other files, which may use >=ES2015 syntax, should only be loaded
 * asynchronously after this version check has been performed.
 */

// eslint-disable-next-line no-var
var semver = require('semver');

// Exit early if the user's node version is too low.
/* istanbul ignore next */
if (!semver.satisfies(process.version, '>=6')) {
  console.log(
      'The Do Lint CLI requires at least Node v6. ' +
      'You have ' + process.version + '.');
  process.exit(1);
}

// Ok, safe to load ES2015.
require('../src/run.js');
