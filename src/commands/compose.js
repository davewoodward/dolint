const compose = require('../compose/index.js');

exports.command = 'compose <filename>';
exports.describe = 'Validates the filename specified against a set of custom Docker Compose rules.';

exports.builder = (yargs) => {
  return yargs.option('i', {
    alias: 'ignore',
    describe: 'Ignore YAML files that don\'t match the Docker Compose file schema.',
    type: 'boolean',
    default: false
  });
};

exports.handler = (argv) => {
  compose.process(argv.filename, argv.c, argv.i);
  process.exit(0);
};
