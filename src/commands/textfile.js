const jenkinsfile = require('../textfile/index.js');

exports.command = 'textfile <filename>';
exports.describe = 'Validates the filename specified against a set of custom rules.';

exports.handler = (argv) => {
  jenkinsfile.process(argv.filename, argv.c);
  process.exit(0);
};
