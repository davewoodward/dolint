const utilities = require('../shared/utilities.js');

module.exports = (filename, serviceName, service, map, rule) => {
  // Validate Rule Structure
  if (rule.path && rule.errorMessage && (rule.regex || rule['exists'] !== undefined)) {
    const target = utilities.deepFind(service, rule.path);
    if (target && rule.regex) {
      // Regex Compare Rule
      let found = false;
      for (let j = 0; j < target.length; j++) {
        const regEx = new RegExp(rule.regex, 'g');
        if (target[j].match(regEx)) {
          found = true;
          break;
        }
      }
      if (!found) {
        const errorMessage = rule.errorMessage.replace('SERVICE_NAME', serviceName).replace('FILENAME', filename);
        const targetName = rule.path.substring(rule.path.lastIndexOf('.') + 1);
        return `${filename}:${map[`${serviceName}_${targetName}`]}:1: ${errorMessage}`;
      } else {
        return;
      }
    } else if ((!target && (rule.regex || rule['exists'] && rule['exists'] == true)) || (target && rule['exists'] !== undefined && rule['exists'] === false)) {
      // The specified field doesn't exist and either a regex was specified or "exists = true" was specified or...
      // The specified field does exist and "exists = false" was specified.
      const lineNumber = utilities.findLine(serviceName, service, rule.path, map);
      const errorMessage = rule.errorMessage.replace('SERVICE_NAME', serviceName).replace('FILENAME', filename);
      return `${filename}:${lineNumber}:1: ${errorMessage}`;
    } else {
      // The field doesn't exist and "exists = false" was specified.
      return;
    }
  } else {
    throw new Error('Invalid rule definition; path, error message, and either a regular expression or exists indicator are required.');
  }
};
