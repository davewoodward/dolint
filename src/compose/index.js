const yaml = require('yaml');
const fs = require('fs');
const composeValidator = require('./validator.js');
const path = require('path');

module.exports.process = (composeFilename, configFilename, ignore) => {
  if (configFilename || fs.existsSync('./dolint.json')) {
    if (!configFilename || fs.existsSync(configFilename)) {
      // Load Linting Rules
      configFilename = configFilename ? configFilename : path.resolve('./dolint.json');
      const configFile = fs.readFileSync(configFilename);

      const config = JSON.parse(configFile);

      // Load Compose File
      if (fs.existsSync(composeFilename)) {
        const file = fs.readFileSync(composeFilename, 'utf8');
        const parsedComposeFile = yaml.parse(file);
        composeValidator.validate(composeFilename, parsedComposeFile, configFilename, config, ignore);
      } else {
        throw new Error(`${composeFilename}:0:0: The file does not exist.`);
      }
    } else {
      throw new Error(`The specified configuration file '${configFilename}' does not exist!`);
    }
  } else {
    throw new Error(`No configuration file found!  Either pass the path and filename using the configuration parameter or place a dolint.json file containing validation rules in ${process.cwd()}.`);
  }
};
