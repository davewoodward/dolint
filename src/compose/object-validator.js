const utilities = require('../shared/utilities.js');

module.exports = (filename, serviceName, service, map, rule) => {
  if (rule.path && rule.errorMessage && (rule['exists'] !== undefined || rule['regex'] !== undefined)) {
    const target = utilities.deepFind(service, rule.path);

    // ************************************************************************
    // The target field exists but shouldn't or the target field doesn't exist
    // but should. The latter could be because of an exists clause or the need
    // to compare it to a regular expression.
    // ************************************************************************
    if ((target && rule['exists'] !== undefined && !rule.exists) || (!target && rule.exists) || (!target && rule['regex'] !== undefined)) {
      return createErrorMessage(filename, serviceName, service, rule, map);
    } else if (target && rule['regex'] !== undefined) {
      const regEx = new RegExp(rule.regex, 'g');
      if (!target.match(regEx)) {
        return createErrorMessage(filename, serviceName, service, rule, map);
      } else {
        return;
      }
    } else {
      return;
    }
  } else {
    throw new Error('Invalid rule definition; path, error message, and either a regular expression or exists indicator are required.');
  }
};

/**
 * @param  {string} filename
 * @param  {string} serviceName
 * @param  {object} service
 * @param  {object} rule
 * @param  {object} map
 * @return {error} results
 */
function createErrorMessage(filename, serviceName, service, rule, map) {
  const lineNumber = utilities.findLine(serviceName, service, rule.path, map);
  const errorMessage = rule.errorMessage.replace('SERVICE_NAME', serviceName).replace('FILENAME', filename);
  return `${filename}:${lineNumber}:1: ${errorMessage}`;
}
