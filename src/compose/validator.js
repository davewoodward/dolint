const utilities = require('../shared/utilities.js');
const logger = require('../shared/logger.js');

module.exports.validate = (composeFilename, composeFile, configFilename, configFile, ignore) => {
  const handlers = {
    array: require('./array-validator.js'),
    object: require('./object-validator.js')
  };

  const lineNumberMap = utilities.createLineNumberHashMap(composeFile);
  if (composeFile && composeFile['services'] && composeFile['version'] && composeFile['version'].includes('3')) {
    let noErrors = true;
    for (const serviceName in composeFile.services) {
      if (composeFile.services.hasOwnProperty(serviceName)) {
        const service = composeFile.services[serviceName];
        if (typeof service === 'object') {
          if (configFile.configurations.compose && configFile.configurations.compose.rules) {
            for (let i = 0; i < configFile.configurations.compose.rules.length; i++) {
              const errorMessage = handlers[configFile.configurations.compose.rules[i].type](composeFilename, serviceName, service, lineNumberMap, configFile.configurations.compose.rules[i]);
              if (errorMessage) {
                noErrors = false;
                logger.error(errorMessage);
              }
            }
          } else {
            logger.warn(`No compose rules defined in ${configFilename}.`);
          }
        } else {
          throw new Error(`${composeFilename}:${lineNumberMap['services']}:1: One or more service definitions is empty!`);
        }
      }
    }

    if (noErrors) {
      logger.info(`The Docker Compose file '${composeFilename}' is Valid`);
    }
  } else if (ignore) {
    logger.warn(`The file '${composeFilename}' is not a valid Docker Compose file. Since ignore was specified this will not be treated as an error.`);
  } else {
    throw new Error(`${composeFilename}:1:1: The file is either not a Docker Compose file or is an unsupported Compose file version (3 or greater required).`);
  }
};
