const yargs = require('yargs');
const logger = require('./shared/logger.js');
const composeLinter = require('./commands/compose.js');
const textfileLinter = require('./commands/textfile.js');

/* istanbul ignore next */
process.on('uncaughtException', (error) => {
  if (error.stack) {
    logger.error(error.stack);
  } else {
    logger.error(error.message);
  }
  process.exit(1);
});

yargs.usage('$0 <command> [options ...]')
    .group(['v', 'h', 'q', 'c'], 'Global Options')
    .help('h', 'print out helpful usage information')
    .alias('h', 'help')
    .option('v', {
      alias: 'verbose',
      describe: 'turn on debugging output',
      type: 'boolean',
      global: true
    })
    .option('q', {
      alias: 'quiet',
      describe: 'silence output',
      type: 'boolean',
      global: true
    })
    .option('c', {
      alias: 'configuration',
      describe: 'Path and filename to a configuration file containing validation rule definitions. By default, the process will look for a dolint.json file in the current working directory.',
      type: 'string',
      global: true
    })
    .command('help', 'Shows this help message, or help for a specific command')
    .command(composeLinter)
    .command(textfileLinter)
    .strict()
    .demandCommand(1, 'You must specify a command to execute.').argv;
