module.exports.info = (message) => {
  console.log('[INFO]', message);
};

module.exports.warn = (message) => {
  console.log('[WARN]', message);
};

module.exports.error = (message) => {
  console.error('[ERROR]', message);
};
