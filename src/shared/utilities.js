module.exports.deepFind = (obj, path) => {
  const paths = path.split('.');
  let current = obj;

  for (let i = 0; i < paths.length; i++) {
    if (current[paths[i]] == undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;
};

/**
 * @param  {object} map
 * @param  {number} nextLine
 * @param  {object} fileContents
 * @param  {string} serviceName
 * @return {object} results
 */
function processObjectGraph(map, nextLine, fileContents, serviceName) {
  for (const name in fileContents) {
    if (fileContents.hasOwnProperty(name)) {
      const indexName = serviceName && serviceName !== 'THIS' ? `${serviceName}_${name}` : name;
      map[indexName] = nextLine;
      nextLine++;
      if (typeof fileContents[name] === 'object') {
        const response = processObjectGraph(map, nextLine, fileContents[name], name === 'services' ? 'THIS' : serviceName === 'THIS' ? name : serviceName);
        nextLine = response.nextLine;
        map = response.map;
      }
    }
  }
  return {
    nextLine,
    map
  };
}

module.exports.createLineNumberHashMap = (fileContents) => {
  const response = processObjectGraph({}, 1, fileContents, false);
  return response.map;
};

module.exports.findLine = (parentName, parentObject, searchPath, map) => {
  const paths = searchPath.split('.');
  let current = parentObject;

  for (let i = 0; i < paths.length; i++) {
    if (current[paths[i]] == undefined) {
      if (i === 0) {
        return map[parentName];
      } else {
        return map[`${parentName}_${paths[i - 1]}`];
      }
    } else {
      current = current[paths[i]];
    }
  }
  return map[`${parentName}_${paths[paths.length - 1]}`];
};
