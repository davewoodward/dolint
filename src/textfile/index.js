const fs = require('fs');
const textfileValidator = require('./validator.js');
const { isText } = require('istextorbinary');
const path = require('path');
const logger = require('../shared/logger.js');

module.exports.process = (filename, configFilename) => {
  if (configFilename || fs.existsSync('./dolint.json')) {
    if (!configFilename || fs.existsSync(configFilename)) {
      // Load Linting Rules
      configFilename = configFilename ? configFilename : path.resolve('./dolint.json');
      const configFile = fs.readFileSync(configFilename);

      const config = JSON.parse(configFile);

      // Load File
      if (fs.existsSync(filename)) {
        if (config.configurations.textfile && config.configurations.textfile.ignore && config.configurations.textfile.ignore.includes(path.extname(filename).replace('.', ''))) {
          logger.info(`Skipping '${filename}' because it's extension is contained within the ignore list in '${configFilename}'.`);
        } else {
          const textfile = fs.readFileSync(filename, 'utf8');
          if (isText(null, textfile)) {
            textfileValidator.validate(filename, textfile, configFilename, config);
          } else {
            throw new Error(`${filename}:0:0: The file specified cannot be compared because it is binary.`);
          }
        }
      } else {
        throw new Error(`${filename}:0:0: The file does not exist.`);
      }
    } else {
      throw new Error(`The specified configuration file '${configFilename}' does not exist!`);
    }
  } else {
    throw new Error(`No configuration file found!  Either pass the path and filename using the configuration parameter or place a dolint.json file containing validation rules in ${process.cwd()}.`);
  }
};
