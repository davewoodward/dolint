const logger = require('../shared/logger.js');

module.exports.validate = (filename, textfile, configFilename, configFile) => {
  let noErrors = true;

  const lines = textfile.split('\n');
  if (configFile.configurations.textfile && configFile.configurations.textfile.rules && configFile.configurations.textfile.rules.length > 0) {
    for (let i = 0; i < configFile.configurations.textfile.rules.length; i++) {
      const rule = configFile.configurations.textfile.rules[i];
      if (rule.regex && rule['exists'] !== undefined && rule.errorMessage) {
        let found = false;
        if (typeof rule.regex === 'object') {
          if (rule.regex.if && rule.regex.then) {
            const comparisons = typeof rule.regex.then === 'object' ? rule.regex.then : [rule.regex.then];
            const regex = new RegExp(rule.regex.if, 'g');
            let conditionMet = false;
            for (let j = 0; j < lines.length; j++) {
              const line = lines[j];
              const position = line.search(regex);
              if (position !== -1) {
                conditionMet = true;
                for (let k = 0; k < comparisons.length; k++) {
                  const regex2 = new RegExp(comparisons[k]);
                  const position2 = line.search(regex2);
                  if (position2 !== -1 && rule['exists'] === false) {
                    noErrors = false;
                    const errorMessage = rule.errorMessage.replace('FILENAME', filename);
                    logger.error(`${filename}:${k + 1}:${position2}: ${errorMessage}`);
                  } else if (position2 !== -1) {
                    found = true;
                  }
                }
              }
            }
            found = found || !conditionMet;
          } else {
            throw new Error(`Invalid rule definition in '${configFilename}'; a regular expression specified as an object must include an 'if' and a 'then' field.`);
          }
        } else {
          const regex = new RegExp(rule.regex, 'g');
          for (let j = 0; j < lines.length; j++) {
            const line = lines[j];
            const position = line.search(regex);
            if (position !== -1 && rule['exists'] === false) {
              noErrors = false;
              const errorMessage = rule.errorMessage.replace('FILENAME', filename);
              logger.error(`${filename}:${j + 1}:${position}: ${errorMessage}`);
            } else if (position !== -1) {
              found = true;
            }
          }
        }

        if (!found && rule['exists'] === true) {
          noErrors = false;
          const errorMessage = rule.errorMessage.replace('FILENAME', filename);
          logger.error(`${filename}:0:0: ${errorMessage}`);
        }
      } else {
        throw new Error(`Invalid rule definition in '${configFilename}'; a regular expression, exists indicator, and an error message are required.`);
      }
    }
  } else {
    logger.warn(`No textfile rules defined in '${configFilename}'.`);
  }

  if (noErrors) {
    logger.info(`The file '${filename}' is Valid`);
  }
};
