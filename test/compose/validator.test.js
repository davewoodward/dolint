const composeValidator = require('../../src/compose/validator.js');
const logger = require('../../src/shared/logger.js');
const assert = require('assert');
const sinon = require('sinon');

describe('The compose module', () => {
  let testData;
  let config;
  let sandbox;
  let info = [];
  let warnings = [];
  let errors = [];

  before(() => {
    sinon.stub(logger, 'info').callsFake((message) => {
      info.push(message);
    });

    sinon.stub(logger, 'warn').callsFake((message) => {
      warnings.push(message);
    });

    sinon.stub(logger, 'error').callsFake((message) => {
      errors.push(message);
    });
  });

  after(() => {
    logger.info.restore();
    logger.warn.restore();
    logger.error.restore();
  });

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    testData = {
      version: '3',
      services: {
        redis1: {
          image: 'redis:alpine',
          deploy: {
            placement: {
              constraints: [
                'node.labels.region == ${GIT_BRANCH}'
              ]
            },
            resources: {
              limits: {
                cpus: '.10',
                memory: '256M'
              },
              reservations: {
                cpus: '0.05',
                memory: '128M'
              }
            }
          }
        },
        redis2: {
          image: 'redis:alpine',
          deploy: {
            placement: {
              constraints: [
                'node.labels.region == ${GIT_BRANCH}'
              ]
            },
            resources: {
              limits: {
                cpus: '.10',
                memory: '256M'
              },
              reservations: {
                cpus: '0.05',
                memory: '128M'
              }
            }
          }
        }
      }
    };

    config = {
      configurations: {
        compose: {
          rules: [
            {
              path: 'deploy.placement.constraints',
              type: 'array',
              regex: 'node\\.labels\\.[a-zA-Z.]*region[a-zA-Z .]*==\\s*((\\${GIT_BRANCH})|development|qa|stage|production)',
              errorMessage: 'No placement constraint found for \'SERVICE_NAME\'. A placement constraint equal to one of the following is required: ${GIT_BRANCH}, develop, qa, stage, or production.'
            },
            {
              path: 'deploy.resources.limits.cpus',
              type: 'object',
              errorMessage: 'No CPU limit found for \'SERVICE_NAME\'. A recommended initial value for this is \'.10\'.',
              exists: true
            },
            {
              path: 'deploy.resources.limits.memory',
              type: 'object',
              errorMessage: 'No memory limit found for \'SERVICE_NAME\'. A recommended initial value for this is \'256M\'.',
              exists: true
            }
          ]
        }
      }
    };

    info = [];
    warnings = [];
    errors = [];
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('validate', () => {
    it('should return success when a compose file is passed containing cpu limits, memory limits, and the GIT_BRANCH variable as a placement constraint.', () => {
      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(info.length === 1 && info[0].match(/^The Docker Compose file 'filename' is Valid$/));
      assert.equal(warnings.length, 0);
      assert.equal(errors.length, 0);
    });

    it('should return an error when one or more services doesn\'t specify a cpu limit.', () => {
      delete testData.services.redis1.deploy.resources.limits.cpus;

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(errors.length === 1 && errors[0].match(/^filename:10:1: No CPU limit found for 'redis1'\. A recommended initial value for this is '\.10'\.$/));
      assert.equal(warnings.length, 0);
      assert.equal(info.length, 0);
    });

    it('should return an error when one or more services doesn\'t specify a memory limit.', () => {
      delete testData.services.redis1.deploy.resources.limits.memory;

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(errors.length === 1 && errors[0].match(/^filename:10:1: No memory limit found for 'redis1'. A recommended initial value for this is '256M'.$/));
      assert.equal(warnings.length, 0);
      assert.equal(info.length, 0);
    });

    it('should return an error when one or more services doesn\'t specify a placement constraint.', () => {
      testData.services.redis2.deploy.placement.constraints = [];

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(errors.length === 1 && errors[0].match(/^filename:20:1: No placement constraint found for 'redis2'. A placement constraint equal to one of the following is required: \${GIT_BRANCH}, develop, qa, stage, or production.$/));
      assert.equal(warnings.length, 0);
      assert.equal(info.length, 0);
    });

    it('should return an error when constraints section is missing.', () => {
      delete testData.services.redis2.deploy.placement.constraints;

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(errors.length === 1 && errors[0].match(/^filename:19:1: No placement constraint found for 'redis2'. A placement constraint equal to one of the following is required: \${GIT_BRANCH}, develop, qa, stage, or production.$/));
      assert.equal(warnings.length, 0);
      assert.equal(info.length, 0);
    });

    it('should return an error when a service definition is empty.', () => {
      testData.services.redis1 = '';

      try {
        composeValidator.validate('filename', testData, 'configFilename', config, false);
        assert.fail('Empty service didn\'t throw an error!');
      } catch (error) {
        assert(error.message.match(/^filename:2:1: One or more service definitions is empty!$/));
        assert.equal(info.length, 0);
        assert.equal(warnings.length, 0);
        assert.equal(info.length, 0);
      }
    });

    it('should return an error when an invalid rule is passed in the config.', () => {
      delete config.configurations.compose.rules[0].path;

      try {
        composeValidator.validate('filename', testData, 'configFilename', config, false);
      } catch (error) {
        assert(error.message.match(/^Invalid rule definition; path, error message, and either a regular expression or exists indicator are required.$/));
        assert.equal(info.length, 0);
        assert.equal(warnings.length, 0);
        assert.equal(info.length, 0);
      }
    });

    it('should return an error when an invalid file is passed and ignore is not specified.', () => {
      const testData = {
        name: 'Fred Flintstone',
        street: '301 Cobblestone Way',
        city: 'Bedrock',
        postal: 70777
      };

      try {
        composeValidator.validate('filename', testData, 'configFilename', config, false);
      } catch (error) {
        assert(error.message.match(/^filename:1:1: The file is either not a Docker Compose file or is an unsupported Compose file version \(3 or greater required\)\.$/));
        assert.equal(info.length, 0);
        assert.equal(warnings.length, 0);
        assert.equal(info.length, 0);
      }
    });

    it('should not return an error when an invalid file is passed and ignore is specified.', () => {
      const testData = {
        name: 'Fred Flintstone',
        street: '301 Cobblestone Way',
        city: 'Bedrock',
        postal: 70777
      };

      composeValidator.validate('filename', testData, 'configFilename', config, true);
      assert(warnings.length === 1 && warnings[0].match(/^The file 'filename' is not a valid Docker Compose file. Since ignore was specified this will not be treated as an error.$/));
      assert.equal(info.length, 0);
      assert.equal(errors.length, 0);
    });

    it('should return success when a field a rule specifying that the \'ports\' field must not be specified and the file passed complies.', () => {
      const errorMessage = 'A port mapping section was specified for \'SERVICE_NAME\'. This is not allowed as Docker Swarm will assign dynamic ports and provide the necessary DNS mapping according to the name(s) specified under the com.docker.lb.hosts label.';
      const config = {
        configurations: {
          compose: {
            rules: [
              {
                path: 'ports',
                type: 'array',
                errorMessage: errorMessage,
                exists: false
              }
            ]
          }
        }
      };

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(info.length === 1 && info[0].match(/^The Docker Compose file 'filename' is Valid$/));
      assert.equal(warnings.length, 0);
      assert.equal(errors.length, 0);
    });

    it('should return an error when a field a rule specifying that the \'ports\' field must be specified and the file passed does not comply.', () => {
      const config = {
        configurations: {
          compose: {
            rules: [
              {
                path: 'ports',
                type: 'array',
                errorMessage: 'A port mapping section was not specified for \'SERVICE_NAME\'.',
                exists: true
              }
            ]
          }
        }
      };

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      const regex = new RegExp('filename:[0-9]*:[0-9]*: A port mapping section was not specified for .*', 'g');
      assert(errors.length === 2 && errors[0].match(regex) && errors[1].match(regex));
      assert.equal(info.length, 0);
      assert.equal(warnings.length, 0);
    });

    it('should return an error when a field a rule specifying that the \'ports\' field must not be specified and the file passed does not comply.', () => {
      const errorMessage = 'A port mapping section was specified for \'SERVICE_NAME\'. This is not allowed as Docker Swarm will assign dynamic ports and provide the necessary DNS mapping according to the name(s) specified under the com.docker.lb.hosts label.';
      const config = {
        configurations: {
          compose: {
            rules: [
              {
                path: 'ports',
                type: 'array',
                errorMessage: errorMessage,
                exists: false
              }
            ]
          }
        }
      };

      testData.services.redis1.ports = ['8080:8080'];

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      const regex = new RegExp(`filename:16:1: ${errorMessage.replace('SERVICE_NAME', 'redis1').replace('.', '\\.').replace('(', '\\(').replace(')', '\\)')}`, 'g');
      assert(errors.length === 1 && errors[0].match(regex));
      assert.equal(info.length, 0);
      assert.equal(warnings.length, 0);
    });

    it('should return success when the compose file complies with the specified object validation.', () => {
      const config = {
        configurations: {
          compose: {
            rules: [
              {
                path: 'deploy.restart_policy.condition',
                type: 'object',
                errorMessage: 'Illegal restart_policy condition specified for \'SERVICE_NAME\'; only \'on-failure\' is allowed.',
                regex: 'on-failure'
              }
            ]
          }
        }
      };
      testData.services.redis1.deploy.restart_policy = {
        condition: 'on-failure'
      };
      testData.services.redis2.deploy.restart_policy = {
        condition: 'on-failure'
      };

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(info.length === 1 && info[0].match(/^The Docker Compose file 'filename' is Valid$/));
      assert.equal(warnings.length, 0);
      assert.equal(errors.length, 0);
    });

    it('should return an error when the compose file does not comply with the specified object validation.', () => {
      const config = {
        configurations: {
          compose: {
            rules: [
              {
                path: 'deploy.restart_policy.condition',
                type: 'object',
                errorMessage: 'Illegal restart_policy condition specified for \'SERVICE_NAME\'; only \'on-failure\' is allowed.',
                regex: 'on-failure'
              }
            ]
          }
        }
      };
      testData.services.redis1.deploy.restart_policy = {
        condition: 'any'
      };
      testData.services.redis2.deploy.restart_policy = {
        condition: 'on-failure'
      };

      composeValidator.validate('filename', testData, 'configFilename', config, false);
      assert(errors.length === 1 && errors[0].match(/^filename:17:1: Illegal restart_policy condition specified for 'redis1'; only 'on-failure' is allowed\.$/));
      assert.equal(info.length, 0);
      assert.equal(warnings.length, 0);
    });
  });
});
