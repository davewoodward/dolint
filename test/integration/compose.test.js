const path = require('path');
const exec = require('child-process-promise').exec;
const assert = require('assert');

describe('command(compose)', () => {
  it('should return a missing argument error message when the user neglects to provide a filename.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} compose`).then(() => {
      return new Error('Allowed to pass an invalid number of arguments!');
    }).catch((result) => {
      assert.equal(result.stdout, '');
      assert(result.stderr.match(/Not enough non-option arguments: got 0, need at least 1/));
      assert.equal(result.code, 1);
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return a file does not exist error message when the user provides an invalid filename.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} compose testData/compose/virtual-file.yml`).then(() => {
      return 'Invalid filename was allowed!';
    }).catch((result) => {
      assert(result.stderr.match(/\[ERROR\] Error: testData\/compose\/virtual-file.yml:0:0: The file does not exist./));
      assert.equal(result.stdout, '');
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return an invalid file error message when a non-YAML file is passed.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} compose testData/compose/not-yaml.yml`).then(() => {
      return 'Invalid filename was allowed!';
    }).catch((result) => {
      assert(result.stderr.match(/\[ERROR\] Error: [a-zA-Z0-9/]*testData\/compose\/not-yaml.yml:1:1: The file is either not a Docker Compose file or is an unsupported Compose file version \(3 or greater required\)/));
      assert.equal(result.stdout, '');
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return success when a compose file is passed containing cpu limits, memory limits, and the GIT_BRANCH variable as a placement constraint.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} compose ${path.resolve('testData/compose/valid.yml')}`).then((result) => {
      assert(result.stdout.match(/\[INFO\] The Docker Compose file '[a-zA-Z0-9/]*testData\/compose\/valid\.yml' is Valid/));
      assert.equal(result.stderr, '');
    });
  });
});
