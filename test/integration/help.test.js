const path = require('path');
const exec = require('child-process-promise').exec;
const assert = require('assert');

describe('command(help)', () => {
  it('should return the full help message when the user fails to pass a command to execute.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')}`).then(() => {
      return new Error('Help was not presented to the user!');
    }).catch((result) => {
      assert.equal(result.stdout, '');
      assert(result.stderr.match(/Commands:/));
    }).then((error) => {
      if (error) throw error;
    });
  });
});
