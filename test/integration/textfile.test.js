const path = require('path');
const exec = require('child-process-promise').exec;
const assert = require('assert');

describe('command(textfile)', () => {
  it('should return a missing argument error message when the user neglects to provide a filename.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} textfile`).then(() => {
      return new Error('Allowed to pass an invalid number of arguments!');
    }).catch((result) => {
      assert.equal(result.stdout, '');
      assert(result.stderr.match(/Not enough non-option arguments: got 0, need at least 1/));
      assert.equal(result.code, 1);
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return a file does not exist error message when the user provides an invalid filename.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} textfile testData/textfile/virtual-file.txt`).then(() => {
      return 'Invalid filename was allowed!';
    }).catch((result) => {
      assert(result.stderr.match(/\[ERROR\] Error: testData\/textfile\/virtual-file.txt:0:0: The file does not exist./));
      assert.equal(result.stdout, '');
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return an invalid file error message when a binary file is passed with an extension that is not in the ignore list.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} textfile ${path.resolve('testData/textfile/intel-logo.jpg')}`).then((response) => {
      console.log(response);
      return 'Invalid file format was allowed!';
    }).catch((result) => {
      assert(result.stderr.match(/\[ERROR\] Error: [a-zA-Z/]*\/testData\/textfile\/intel-logo\.jpg:0:0: The file specified cannot be compared because it is binary\./));
      assert.equal(result.stdout, '');
    }).then((error) => {
      if (error) throw error;
    });
  });

  it('should return success when a binary file is passed with an extension that matches an entry in the ignore list.', () => {
    return exec(`node ${path.resolve('bin/dolint.js')} textfile ${path.resolve('testData/textfile/moby-dock.png')}`).then((result) => {
      assert(result.stdout.match(/\[INFO\] Skipping '[a-zA-Z/]*\/testData\/textfile\/moby-dock\.png' because it's extension is contained within the ignore list in '[a-zA-Z/.]*/));
      assert.equal(result.stderr, '');
    });
  });
});
