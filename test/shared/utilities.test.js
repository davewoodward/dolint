const assert = require('assert');
const utilities = require('../../src/shared/utilities.js');

describe('The utilities module', () => {
  it('should return the line associated with the parent object when none of the objects identified in the path exist.', () => {
    const map = {
      fieldOne: 1
    };

    const testData = {
      fieldOne: {
      }
    };

    assert.equal(utilities.findLine('fieldOne', testData.fieldOne, 'fieldTwo.fieldThree.fieldFour', map), 1);
  });
});
