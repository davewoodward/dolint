const textfileValidator = require('../../src/textfile/validator.js');
const logger = require('../../src/shared/logger.js');
const assert = require('assert');
const sinon = require('sinon');

describe('The textfile module', () => {
  let config;
  let sandbox;
  let info = [];
  let warnings = [];
  let errors = [];
  let Jenkinsfile;

  before(() => {
    sinon.stub(logger, 'info').callsFake((message) => {
      info.push(message);
    });

    sinon.stub(logger, 'warn').callsFake((message) => {
      warnings.push(message);
    });

    sinon.stub(logger, 'error').callsFake((message) => {
      errors.push(message);
    });
  });

  after(() => {
    logger.info.restore();
    logger.warn.restore();
    logger.error.restore();
  });

  beforeEach(() => {
    sandbox = sinon.createSandbox();

    Jenkinsfile = 'pipeline {\n';
    Jenkinsfile += '  agent any\n';
    Jenkinsfile += '  stages {\n';
    Jenkinsfile += '    stage("Deploy") {\n';
    Jenkinsfile += '      when {\n';
    Jenkinsfile += '        beforeAgent true\n';
    Jenkinsfile += '        expression { BRANCH_NAME ==~ /(production|stage|qa|development)/ }\n';
    Jenkinsfile += '      }\n';
    Jenkinsfile += '      environment {\n';
    Jenkinsfile += '        DOCKER_HOST = "${_DOCKER_HOST}"\n';
    Jenkinsfile += '        DOCKER_TLS_VERIFY = "${_DOCKER_TLS_VERIFY}"\n';
    Jenkinsfile += '        DOCKER_CERT_PATH = "${_DOCKER_CERT_PATH}"\n';
    Jenkinsfile += '        GIT_BRANCH = "production"\n';
    Jenkinsfile += '      }\n';
    Jenkinsfile += '      steps {\n';
    Jenkinsfile += '        sh "docker stack deploy -c compose.yml ${APPLICATION}-${GIT_BRANCH}"\n';
    Jenkinsfile += '      }\n';
    Jenkinsfile += '    }\n';
    Jenkinsfile += '  }\n';
    Jenkinsfile += '}\n';

    config = {
      configurations: {
        textfile: {
          ignore: [
            'jpg',
            'jpeg',
            'png'
          ],
          rules: [
            {
              description: 'Do not allow anyone to override the GIT_BRANCH environment variable',
              regex: '.*GIT_BRANCH.*=',
              exists: false,
              errorMessage: 'The file "FILENAME" is attempting to override the GIT_BRANCH environment variable.  This is NOT allowed!'
            }
          ]
        }
      }
    };

    info = [];
    warnings = [];
    errors = [];
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('validate', () => {
    it('should return success when a jenkinsfile is passed that does not attempt to override the GIT_BRANCH environment variable.', () => {
      textfileValidator.validate('Jenkinsfile', Jenkinsfile, 'configFilename', config);
      assert(errors.length === 1 && errors[0].match(/^Jenkinsfile:13:0: The file "Jenkinsfile" is attempting to override the GIT_BRANCH environment variable. {2}This is NOT allowed!$/));
      assert.equal(info.length, 0);
      assert.equal(warnings.length, 0);
    });

    it('should return success and a warning when a configuration without textfile rules is passed.', () => {
      config.configurations.textfile.rules = [];
      textfileValidator.validate('Jenkinsfile', Jenkinsfile, 'configFilename', config);
      assert(info.length === 1 && info[0].match(/^The file 'Jenkinsfile' is Valid$/));
      assert(warnings.length === 1 && warnings[0].match(/^No textfile rules defined in 'configFilename'.$/));
      assert.equal(errors.length, 0);
    });

    it('should return an error when an invalid rule definition is passed.', () => {
      delete config.configurations.textfile.rules[0].regex;

      try {
        textfileValidator.validate('Jenkinsfile', Jenkinsfile, 'configFilename', config);
      } catch (error) {
        assert(error.message.match(/^Invalid rule definition in 'configFilename'; a regular expression, exists indicator, and an error message are required.$/));
        assert.equal(info.length, 0);
        assert.equal(warnings.length, 0);
        assert.equal(info.length, 0);
      }
    });
  });
});
